import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ToDoComponent } from './to-do/to-do.component';
import { TableComponent } from './table/table.component';


const routes: Routes = [{
  path: '',
  pathMatch: 'full',
  component: TableComponent
},
{
  path: 'todo',
  component: ToDoComponent
}];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
