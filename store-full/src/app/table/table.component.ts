import { Component, OnInit } from "@angular/core";
import { Observable } from "rxjs";
import TableState from "../state/table.state";
import { Store, select } from "@ngrx/store";
import * as TableActions from "../actions/table.action";
import { tableUsersSelector } from "../selectors/selectors";
@Component({
  selector: "app-table",
  templateUrl: "./table.component.html",
  styleUrls: ["./table.component.scss"],
})
export class TableComponent implements OnInit {
  public todo$: Observable<TableState>;

  constructor(private store: Store<TableState>) {}

  ngOnInit(): void {
    this.store.select(tableUsersSelector).subscribe((users) => {
      console.log(users);
    });

    this.store.dispatch(TableActions.GetTableDataAction());
  }
}
