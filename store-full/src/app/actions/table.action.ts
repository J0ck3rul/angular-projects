import { createAction, props } from "@ngrx/store";
import { Table } from "../models/table.model";
export const GetTableDataAction = createAction(
  "[Table] - Get Data",
);

export const SuccessGetTableDataAction = createAction(
  "[Table] - Success Get Data",
  props<{table: Table}>()
)
