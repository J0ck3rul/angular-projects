import { createFeatureSelector, createSelector } from '@ngrx/store';
import TableState from '../state/table.state';

export const tableSelector = createFeatureSelector('table');
export const tableUsersSelector = createSelector(tableSelector, (state: TableState) => {
  return state.users;
})
