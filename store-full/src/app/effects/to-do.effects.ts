import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Actions, createEffect, ofType } from "@ngrx/effects";
import { Observable, of } from "rxjs";
import { Action } from "@ngrx/store";
import * as ToDoActions from "../actions/to-do.action";
import { mergeMap, map, catchError } from "rxjs/operators";
import ToDo from "../to-do/to-do.component";

@Injectable()
export class ToDoEffects {
  constructor(private http: HttpClient, private actions$: Actions) {}

  private ApiURL: string = "https://localhost:44308/api/ToDo";

  GetToDos$: Observable<Action> = createEffect(() =>
    this.actions$.pipe(
      ofType(ToDoActions.BeginGetToDoAction),
      mergeMap((action) =>
        this.http.get(this.ApiURL).pipe(
          map((data: ToDo[]) => {
            return ToDoActions.SuccessGetToDoAction({ payload: data });
          }),
          catchError((error: Error) => {
            return of(ToDoActions.ErrorToDoAction(error));
          })
        )
      )
    )
  );

  CreateToDos$: Observable<Action> = createEffect(() =>
    this.actions$.pipe(
      ofType(ToDoActions.BeginCreateToDoAction),
      mergeMap((action) =>
        this.http
          .post(this.ApiURL, JSON.stringify(action.payload), {
            headers: { "Content-Type": "application/json" },
          })
          .pipe(
            map((data: ToDo) => {
              return ToDoActions.SuccessCreateToDoAction({ payload: data });
            }),
            catchError((error: Error) => {
              return of(ToDoActions.ErrorToDoAction(error));
            })
          )
      )
    )
  );
}
