import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Actions, ofType, createEffect } from "@ngrx/effects";
import { Observable, of } from "rxjs";
import * as TableActions from "../actions/table.action";
import { Action } from "@ngrx/store";
import { mergeMap, map } from "rxjs/operators";
import { Table } from '../models/table.model';

@Injectable()
export class TableEffects {
  constructor(private http: HttpClient, private actions$: Actions) {}

  private url: string = "https://jsonplaceholder.typicode.com/users";

  GetTable$ = createEffect(() =>
    this.actions$.pipe(
      ofType(TableActions.GetTableDataAction),
      map(() => TableActions.SuccessGetTableDataAction({table: {username: '', id: '', email: ''}}))
    )
  );
}
