import ToDo from "../to-do/to-do.component";

export default class ToDoState {
  ToDos: Array<ToDo>;
  ToDoError: Error;
}

export const initializeState = (): ToDoState => {
  return { ToDos: Array<ToDo>(), ToDoError: null};
};


