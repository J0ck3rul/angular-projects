import { Table } from '../models/table.model';

export default interface TableState {
  users: Table[];
}

export const initialState: TableState = {users: []};
