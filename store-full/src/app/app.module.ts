import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { FormsModule } from "@angular/forms";

import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { ToDoComponent } from "./to-do/to-do.component";
import { StoreModule } from "@ngrx/store";
import { ToDoReducer } from "./reducers/to-do.reducer";
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { TableComponent } from './table/table.component';
import { TableReducer } from './reducers/table.reducer';
import { EffectsModule } from '@ngrx/effects';
import { TableEffects } from './effects/table.effects';

@NgModule({
  declarations: [AppComponent, ToDoComponent, TableComponent],
  imports: [
    BrowserModule,
    AppRoutingModule,
    StoreModule.forRoot({ todos: ToDoReducer, table: TableReducer }),
    EffectsModule.forRoot([TableEffects]),
    FormsModule,
    CommonModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
