import { Component, OnInit } from "@angular/core";
import ToDoState from "../state/to-do.state";
import { Observable, Subscription } from "rxjs";
import { map } from "rxjs/operators";
import { Store, select } from "@ngrx/store";
import * as ToDoActions from "../actions/to-do.action";

export default class ToDo {
  title: string;
  isCompleted: boolean;
}

@Component({
  selector: "app-to-do",
  templateUrl: "./to-do.component.html",
  styleUrls: ["./to-do.component.css"],
})
export class ToDoComponent implements OnInit {
  public todo$: Observable<ToDoState>;
  public ToDoSubscription: Subscription;
  public ToDoList: ToDo[] = [];

  public Title: string;
  public IsCompleted: boolean = false;

  public toDoError: Error = null;

  constructor(private store: Store<{ todos: ToDoState }>) {
    this.todo$ = store.pipe(select("todos"));
  }

  ngOnInit() {
    this.ToDoSubscription = this.todo$
      .pipe(
        map((x: ToDoState) => {
          this.ToDoList = x.ToDos;
          this.toDoError = x.ToDoError;
        })
      )
      .subscribe();

    this.store.dispatch(ToDoActions.BeginGetToDoAction());
  }

  public createToDo(): void {
    const todo: ToDo = { title: this.Title, isCompleted: this.IsCompleted };

    this.store.dispatch(ToDoActions.BeginCreateToDoAction({payload: todo}))

    this.Title = '';
    this.IsCompleted = false;
  }

  ngOnDestroy(): void {
    if(this.ToDoSubscription) {
      this.ToDoSubscription.unsubscribe();
    }

  }
}
