import { createReducer, on, Action } from "@ngrx/store";
import * as TableActions from "../actions/table.action";
import TableState, { initialState } from "../state/table.state";

const tableReducer = createReducer(
  initialState,
  on(TableActions.GetTableDataAction, (state: TableState) => {
    return { ...state };
  }),
  on(TableActions.SuccessGetTableDataAction, (state: TableState, { table }) => {
    return { ...state, users: [...state.users, table] };
  })
);

export function TableReducer(state: TableState | undefined, action: Action) {
  return tableReducer(state, action);
}
//  se da trigger?
