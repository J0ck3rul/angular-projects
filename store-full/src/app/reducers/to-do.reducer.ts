import ToDoState, { initializeState } from "../state/to-do.state";
import { createReducer, on, Action } from "@ngrx/store";
import * as ToDoActions from "../actions/to-do.action";
import ToDo from "../to-do/to-do.component";

export const initialState = initializeState();

const reducer = createReducer(
  initialState,
  on(ToDoActions.GetToDoAction, (state) => state),
  on(ToDoActions.CreateToDoAction, (state: ToDoState, toDo: ToDo) => {
    return { ...state, ToDos: [...state.ToDos, toDo], ToDoError: null };
  }),
  on(ToDoActions.SuccessGetToDoAction, (state: ToDoState, { payload }) => {
    return { ...state, ToDos: payload };
  }),
  on(ToDoActions.SuccessCreateToDoAction, (state: ToDoState, { payload }) => {
    return { ...state, ToDos: [...state.ToDos, payload], ToDoError: null };
  }),
  on(ToDoActions.ErrorToDoAction, (state: ToDoState, error: Error) => {
    console.log(error);
    return { ...state, ToDoError: error };
  })
);

export function ToDoReducer(state: ToDoState | undefined, action: Action) {
  return reducer(state, action);
}
